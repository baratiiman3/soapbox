export enum Entities {
  ACCOUNTS = 'Accounts',
  GROUPS = 'Groups',
  GROUP_RELATIONSHIPS = 'GroupRelationships',
  GROUP_MEMBERSHIPS = 'GroupMemberships',
  RELATIONSHIPS = 'Relationships'
}